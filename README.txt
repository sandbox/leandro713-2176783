Pin Module README
=================

This module implements a 2-factor admin authentication method via jabber.

It's not intended for regular users, it's only one more security factor for
administrators authentication.

The way this works is: you fill in your user credentials at /user and you will
get via jabber a pin code. Afterwards you need to enter this code at /pin_login
to authenticate yourself.

Beware of activating this module without testing the jabber account configuration,
otherwise you'll need to disable this module to do a regular login.

REQUIREMENTS
============

* A jabber account or a gmail gtalk account (NOT including google apps accounts)
  Caution: currently we don't support SRV DNS Lookups

* http://code.google.com/p/xmpphp/ as library


INSTALLATION & CONFIGURATION
============================

* Download and move XMPPHP in sites/all/libraries
  [tested release is https://code.google.com/p/xmpphp/downloads/detail?name=xmpphp-0.1rc2-r77.tgz]

* Download this module to that place where you place contrib modules

* Move the file «XMPPHP.libraries.info» to sites/all/libraries

* Configure your jabber account settings in /admin/settings/admin_pin

  When you save your jabber configuration you will get a test message
  (just remembrer to connect your jabber account).

  If you get correctly this test jabber message you can tick the «Activate?» checkbox
  and save definitively the configuration.

* All is ready. Next login will send you a pin code via jabber that you will need to login
  at /pin_login


MORE INFO & HOWTO
=================
I did a video in spanish explaining how it works www.youtube.com/embed/OEECgKVc8fI

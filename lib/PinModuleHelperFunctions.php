<?php

namespace Drupal\pin;

/**
 * PinModuleHelperFunctions Helper for PIN module.
 *
 *
 * @version 1.0
 * @author David
 */
class PinModuleHelperFunctions {
  static function SendMessage($msg) {

    $library = libraries_load('XMPP');
    if ($library && !empty($library['loaded'])) {
	  // dpr($library);die();
    }
    else {
      // @todo watchdog
      return;
    }

    if (!variable_get('pin_username')) {
      return;
    }

    $conn = new \XMPPHP_XMPP(variable_get('pin_server'),
                variable_get('pin_port'),
                variable_get('pin_username'),
                variable_get('pin_password'),
                'xmpphp',
                variable_get('pin_domain'),
                $printlog = FALSE,
                $loglevel = \XMPPHP_Log::LEVEL_INFO);

    try {

      $conn->connect();
      $conn->processUntil('session_start');
      $conn->presence();
      $conn->message(variable_get('pin_username') . "@" . variable_get('pin_domain'), $msg);
      $conn->disconnect();
      return 1;

    }
    catch(\XMPPHP_Exception $e) {

      die($e->getMessage());

    }
  }
}
